<?php global $page;

// Add Shortcode
function button_shortcode( $atts ) {

	// Attributes
	$atts = shortcode_atts( array( 'text' => 'Tekst voor op de knop', 'url' => '', 'section' => '', ), $atts );

	return ( $atts['url'] !== '' && $atts['section'] == '' ) ?
		'<a target="_blank" href="' . $atts['url'] . '" class="btn">' . $atts['text'] . '</a>' :
		'<a data-href="' . $atts['section'] . '" class="btn">' . $atts['text'] . '</a>';

	/***************
	 * Url: [button url="http://google.nl" text="Neem contact op"]
	 *
	 * Section [button section="diensten" text="Neem contact op"]
	 * Keuze uit: diensten, projecten, contact
	 *
	 */

}

add_shortcode( 'button', 'button_shortcode' );

// AJAX STUFF
function my_enqueue_assets() {
	global $wp_query;
	// Add scripts to page
	wp_enqueue_script( 'gmaps_script', get_template_directory_uri() . "/js/gmaps.min.js" );
	wp_enqueue_script( 'main_script', get_template_directory_uri() . "/js/script.min.js", array( 'jquery' ), '1.0', true );
	// add ajaxurl js local variable to page
	wp_localize_script( 'main_script', 'ajaxpagination', array(
		'ajaxurl' => admin_url( 'admin-ajax.php' )
	) );
}

add_action( 'wp_enqueue_scripts', 'my_enqueue_assets' );

function my_main_script() {
	$p = get_post( $_POST['id'] );
	$p->featured_image = get_the_post_thumbnail_url( $_POST['id'], 'full' );    // get full src url
	echo json_encode( $p );
}

add_action( 'wp_ajax_nopriv_main_script', 'my_main_script' );
add_action( 'wp_ajax_main_script', 'my_main_script' );

// Hide adminbar on front-end
show_admin_bar( false );

// Add thumbnail/featured-image support
if ( function_exists( 'add_theme_support' ) ) {
	add_theme_support( 'post-thumbnails' );
}

// Add the sub_admin role
remove_role( 'sub_admin' );
$exist_dealer_role = get_role( 'sub_admin' );
if ( empty( $exist_dealer_role ) ) {
	add_role( 'sub_admin', 'Mede beheerder', array(
		'read'                   => true, // Access to Dashboard and Users -> Your Profile.
		'update_core'            => true, // Can update core. I added a plugin for this.
		'edit_posts'             => true, //Access to Posts, Add New, Comments and moderating comments.
		// Posts
		'edit_posts'             => true, //Access to Posts, Add New, Comments and moderating comments.
		'create_posts'           => true, // Allows user to create new posts
		'delete_posts'           => true, // Can delete posts.
		'publish_posts'          => true, // Can publish posts. Otherwise they stay in draft mode.
		'delete_published_posts' => true, // Can delete published posts.
		'edit_published_posts'   => true, // Can edit posts.
		'edit_others_posts'      => true, // Can edit other users posts.
		'delete_others_posts'    => true, // Can delete other users posts.
		// Categories, comments and users
		'manage_categories'      => true, // Access to managing categories.
		'moderate_comments'      => true, // Access to moderating comments. Edit posts also needs to be set to true.
		'edit_comments'          => true, // Comments are blocked out for this user.
		'edit_users'             => true, // Can view other users.
		'create_users'           => true, // Can create users.
		'delete_users'           => true, // Can delete users.
		'remove_users'           => true, // Can remove users.
		'list_users'             => true, // Can list users.
		// Pages
		'edit_pages'             => true, // Access to Pages and Add New (page).
		'publish_pages'          => true, // Can publish pages.
		'edit_others_pages'      => true, // Can edit other users pages.
		'edit_published_pages'   => true, // Can edit published pages.
		'delete_pages'           => true, // Can delete pages.
		'delete_others_pages'    => true, // Can delete other users pages.
		'delete_published_pages' => true, // Can delete published pages.
		// Media Library
		'upload_files'           => true, // Access to Media Library.
		// Appearance
		'edit_theme_options'     => true, // Access to Appearance panel options.
		'switch_themes'          => false, // Can switch themes.
		'delete_themes'          => false, // Can delete themes.
		'install_themes'         => false, // Can install a new theme.
		'update_themes'          => false, // Can update themes.
		'edit_themes'            => true, // Can edit themes - through the appearance editor.
		// Plugins
		'activate_plugins'       => false, // Access to plugins screen.
		'edit_plugins'           => false, // Can edit plugins - through the appearance editor.
		'install_plugins'        => false, // Access to installing a new plugin.
		'update_plugins'         => false, // Can update plugins.
		'delete_plugins'         => false, // Can delete plugins.
		// Settings
		'manage_options'         => true, // Can access Settings section.
		// Tools
		'import'                 => false, // Can access Tools section.
	) );
}

// Custom function to limit content
function content( $content = null, $limit ) {
	if ( $content == null ) {
		$content = explode( ' ', get_the_content(), $limit );
	} else {
		$content = explode( ' ', $content, $limit );
	}

	if ( count( $content ) >= $limit ) {
		array_pop( $content );
		$content = implode( " ", $content ) . '...';
	} else {
		$content = implode( " ", $content );
	}
	$content = preg_replace( '/[.+]/', '', $content );
	//	$content = apply_filters('the_content', $content);
	$content = str_replace( ']]>', ']]&gt;', $content );

	return $content;
}