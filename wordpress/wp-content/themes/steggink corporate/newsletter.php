<?php

/*
 * Template Name: Newsletter
 */

get_header('newsletter'); ?>

	<div class="window active parallax" data-order="1">
	<div class="landing parallax__layer parallax__layer--base">
		<div class="summary">
			<h2 class="title"><?php echo get_bloginfo('name'); ?></h2>
			<?php echo get_bloginfo('description'); ?>
		</div>
		<div class="scrollerMouse">
			<img src="<?php echo get_template_directory_uri(); ?>/oud/images/mouse.png" alt="">
			<span>Scroll naar beneden</span>
		</div>
	</div>
	<section>
		<div class="inner">
		<?php if ( have_posts() ): ?>
			<?php while ( have_posts() ): the_post(); ?>
				<h2><?php the_title(); ?></h2>
				<?php the_content(); ?>
			<?php endwhile; ?>
		<?php endif; ?>
		</div>
	</section>
<?php get_footer(); ?>