<?php $ID = get_option( 'page_on_front' ); ?>

<footer id="contact" class="parallax__layer parallax__layer--base">
    <div class="inner cols">
        <div class="col">
            <h3>Contactgegevens</h3>
            <p><?php the_field( 'adres', $ID ); ?><br/>
				<?php the_field( 'plaats_postcode', $ID ); ?></p>
			<?php if ( get_field( 'telefoon', $ID ) ): ?>
                <a href="tel:<?php the_field( 'telefoon', $ID ); ?>" class="btn"><?php the_field( 'telefoon', $ID ); ?></a><?php endif; ?>
            <a href="mailto:<?php the_field( 'email', $ID ); ?>" class="btn"><?php the_field( 'email', $ID ); ?></a>
            <div class="btn" id="requestContact">Neem contact op</div>
            <div class="social-media">
                <a href="http://facebook.com/<?php the_field( 'facebook', $ID ); ?>">
                    <div class="facebook"></div>
                </a>
                <a href="http://twitter.com/<?php the_field( 'twitter', $ID ); ?>">
                    <div class="twitter"></div>
                </a>
                <a href="http://linkedin.com/<?php the_field( 'linkedin', $ID ); ?>">
                    <div class="linkedin"></div>
                </a>
            </div>
        </div>
		<?php // Streetview
        $map = get_field( 'maps' ); ?>
        <div id="map1" class="googleMap" data-lat="<?php echo $map['lat']; ?>" data-lng="<?php echo $map['lng']; ?>" data-title="<?php echo get_bloginfo( 'name' ); ?>"></div>
    </div>
</footer>
<div class="shade"></div>
<div class="video">
	<?php $video = ( get_field( 'bestand_of_url', $ID ) == 'url' ) ? get_field( 'url', $ID ) : get_field( 'bestand', $ID ); ?>
    <video id="video1" class="video-js" preload autoplay width="100%" loop="true" muted="true" data-setup="">
        <source src="<?php echo $video; ?>"/>
        <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web
            browser that<a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5
                video</a></p>
    </video>
</div>
</div>
</div>
</div>

<?php wp_footer(); ?>

<!-- Google Maps API -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDy73PHHhM5POMmBakQzEefFvCpaX63lo4&callback=initMap" async defer></script>

<script>
    var panorama;

    function initMap() {
        if ($('.googleMap').length > 0) {
            $map = $('.googleMap');
            panorama = new google.maps.StreetViewPanorama(
                document.getElementById($map.attr('id')),
                {
                    position: {lat: $map.data('lat'), lng: $map.data('lng')},
                    pov: {heading: 165, pitch: 0},
                    zoom: 1
                });
        }
    }
</script>

<!-- Video.js -->
<script src="http://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>
<script src="http://vjs.zencdn.net/5.19.1/video.js"></script>

<!--<script src="--><?php //echo get_template_directory_uri(); ?><!--/js/script.min.js"></script>-->

</body>
</html>