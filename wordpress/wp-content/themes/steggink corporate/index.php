<?php get_header();

$ID = get_option( 'front_page' ); ?>

    <div class="window active parallax" data-order="1">
    <div class="landing parallax__layer parallax__layer--base">
        <div class="summary">
            <h2 class="title"><?php echo get_field( 'landing_title', $ID ); ?></h2>
			<?php if ( have_rows( 'quotes_slider' ) ): ?>
                <div id="quoteSlider" class="owl-carousel">
					<?php while ( have_rows( 'quotes_slider' ) ) : the_row(); ?>
                        <div><?php the_sub_field( 'quote' ); ?></div>
					<?php endwhile; ?>
                </div>
			<?php endif; ?>
			<?php echo do_shortcode( '[button section="diensten" text="Bekijk onze diensten"]' ); ?>
            <div id="watchLandingVideo" class="btn mobile-only">Bekijk video</div>
        </div>
        <div class="scrollerMouse">
            <img src="<?php echo get_template_directory_uri(); ?>/oud/images/mouse.png" alt="">
            <span>Scroll naar beneden</span>
        </div>
    </div>
    <section id="diensten" class="diensten no-padding parallax__layer parallax__layer--base">
        <div class="dienstenMenu">
            <div class="inner">
				<?php if ( have_rows( 'diensten' ) ): $i = 0;
					while ( have_rows( 'diensten' ) ) : the_row();
						$i ++; ?>
                        <div class="subject" data-subject-id="<?php echo $i; ?>">
                            <img src="<?php echo get_template_directory_uri(); ?>/oud/images/beeldmerk_round_white.svg" alt="">
                            <span class="subjectTitle"><?php the_sub_field( 'title' ); ?></span>
                        </div>
					<?php endwhile;
				endif; ?>
            </div>
        </div>
        <div class="dienstenContentContainer">
            <i class="fa fa-times"></i>
            <div class="indicator"></div>
			<?php if ( have_rows( 'diensten' ) ): $i = 0;
				while ( have_rows( 'diensten' ) ) : the_row();
					$i ++; ?>
                    <div class="dienstenContent <?php if ( $i == 1 ) {
						echo 'active';
					} ?>" data-subject-id="<?php echo $i; ?>">
                        <div class="inner small">
                            <h2><?php the_sub_field( 'title' ); ?></h2>
                            <div class="subTitle"><?php the_sub_field( 'subTitle' ); ?></div>
							<?php $qwety = get_sub_field( 'images' );
							foreach ( $qwety as $img ): ?>
                                <div class="dienstImage" style="background-image: url(<?php echo $img['url']; ?>);"></div>
							<?php endforeach; ?>
                        </div>
                    </div>
				<?php endwhile;
			endif; ?>
        </div>
    </section>
    <section class="fullWidthImage">
        <div class="parallax__layer parallax__layer--back" style="background-image: url('<?php the_field( 'fullwidthimage1' ); ?>')">
    </section>
    <section id="projecten" class="projects parallax__layer parallax__layer--base">
        <div class="inner">
			<?php
			// WP_Query arguments
			$args = array(
				'post_type'      => array( 'projecten' ),
				'post_status'    => array( 'publish' ),
				'posts_per_page' => '3',
			);

			// The Query
			$query = new WP_Query( $args ); ?>
            <h2>Projecten</h2>
            <div class="projectsContainer">
				<?php if ( $query->have_posts() ) { ?>
				<?php while ( $query->have_posts() ) {
					$query->the_post();
					$thumb_id        = get_post_thumbnail_id();
					$thumb_url_array = wp_get_attachment_image_src( $thumb_id, 'thumbnail-size', true );
					$thumb_url       = $thumb_url_array[0]; ?>
                    <div class="project">
                        <a id="<?php the_ID(); ?>" href="" class="projectImage" style="background-image: url('<?php echo $thumb_url; ?>')"></a>
                        <a id="<?php the_ID(); ?>" href=""><h3><?php the_title(); ?></h3></a>
						<?php $content = apply_filters( 'the_content', $query->post_content );
						echo "<p>" . content( $content, 25 ) . "</p>"; ?>
                        <a class="read-more" id="<?php the_ID(); ?>" href="">Verder lezen</a>
                    </div>
				<?php } ?>
            </div>
            <div class="btn" id="allProjects">Bekijk alle projecten</div>
		<?php } else { ?>
            <p>Er zijn geen projecten te bekijken..</p>
		<?php }
		// Restore original Post Data
		wp_reset_postdata();
		?>
        </div>
    </section>
    <section class="teamPhotoSlider">
        <h2 class="projects parallax__layer parallax__layer--base">Team</h2>
        <div class="parallax__layer parallax__layer--back">
            <div id="teamPhotoSlider" class="owl-carousel">
				<?php $photos = get_field( 'team_slider' );
				foreach ( $photos as $photo ): ?>
                    <img src="<?php echo $photo['url']; ?>"/>
				<?php endforeach; ?>
            </div>
        </div>
    </section>
    <section id="over-ons" class="about parallax__layer parallax__layer--base">
        <div class="inner">
            <h2>Even voorstellen</h2>
			<?php the_field( 'over_ons_tekst' ); ?>
			<?php if ( have_rows( 'team' ) ): ?>
                <div class="team">
					<?php while ( have_rows( 'team' ) ) : the_row(); ?>
                        <div class="member">
                            <div class="picture" style="background-image: url('<?php the_sub_field( 'foto' ); ?>');"></div>
                            <div class="info">
                                <div class="name"><?php the_sub_field( 'naam' ); ?></div>
                                <div class="function"><?php the_sub_field( 'functie' ); ?></div>
                            </div>
                        </div>
					<?php endwhile; ?>
                </div>
			<?php endif; ?>
        </div>
    </section>
    <!--            <section id="contact" class="contact parallax__layer parallax__layer--base">-->
    <!--                </section>-->
<?php get_footer(); ?>