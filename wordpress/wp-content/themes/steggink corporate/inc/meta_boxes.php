<?php

/* Fire our meta box setup function on the post editor screen. */
add_action( 'load-post.php', 'recreate_post_meta_boxes_setup' );
add_action( 'load-post-new.php', 'recreate_post_meta_boxes_setup' );

/* Meta box setup function. */
function recreate_post_meta_boxes_setup() {

	/* Add meta boxes on the 'add_meta_boxes' hook. */
	add_action( 'add_meta_boxes', 'recreate_add_post_meta_boxes' );

	/* Save post meta on the 'save_post' hook. */
	add_action( 'save_post', 'recreate_save_diensten_meta', 10, 2 );
}

/* Create one or more meta boxes to be displayed on the post editor screen. */
function recreate_add_post_meta_boxes() {

	add_meta_box(
		'recreate-diensten',      // Unique ID
		esc_html__( 'Diensten', 'Recreate' ),    // Title
		'recreate_diensten_meta_box',   // Callback function
		'page',         // Admin page (or post type)
		'normal',         // Context
		'default'         // Priority
	);
}

/* Display the post meta box. */
function recreate_diensten_meta_box( $post ) { ?>

	<?php wp_nonce_field( basename( __FILE__ ), 'recreate_diensten_nonce' ); ?>

	<p>
		<label for="recreate-diensten"><?php _e( "Voeg .", 'Recreate' ); ?></label>
		<br />
		<input class="widefat" type="text" name="recreate-diensten" id="recreate-diensten" value="<?php echo esc_attr( get_post_meta( $post->ID, 'recreate_diensten', true ) ); ?>" size="30" />
	</p>
<?php }

/* Save the meta box's post metadata. */
function recreate_save_diensten_meta( $post_id, $post ) {

	/* Verify the nonce before proceeding. */
	if ( !isset( $_POST['recreate_diensten_nonce'] ) || !wp_verify_nonce( $_POST['recreate_diensten_nonce'], basename( __FILE__ ) ) )
		return $post_id;

	/* Get the post type object. */
	$post_type = get_post_type_object( $post->post_type );

	/* Check if the current user has permission to edit the post. */
	if ( !current_user_can( $post_type->cap->edit_post, $post_id ) )
		return $post_id;

	/* Get the posted data and sanitize it for use as an HTML class. */
	$new_meta_value = ( isset( $_POST['recreate-diensten'] ) ? sanitize_html_class( $_POST['recreate-diensten'] ) : '' );

	/* Get the meta key. */
	$meta_key = 'recreate_diensten';

	/* Get the meta value of the custom field key. */
	$meta_value = get_post_meta( $post_id, $meta_key, true );

	/* If a new meta value was added and there was no previous value, add it. */
	if ( $new_meta_value && '' == $meta_value )
		add_post_meta( $post_id, $meta_key, $new_meta_value, true );

	/* If the new meta value does not match the old value, update it. */
	elseif ( $new_meta_value && $new_meta_value != $meta_value )
		update_post_meta( $post_id, $meta_key, $new_meta_value );

	/* If there is no new meta value but an old value exists, delete it. */
	elseif ( '' == $new_meta_value && $meta_value )
		delete_post_meta( $post_id, $meta_key, $meta_value );
}

/* Filter the post class hook with our custom post class function. */
add_filter( 'diensten', 'recreate_diensten' );

function recreate_diensten( $classes ) {

	/* Get the current post ID. */
	$post_id = get_the_ID();

	/* If we have a post ID, proceed. */
	if ( !empty( $post_id ) ) {

		/* Get the custom post class. */
		$diensten = get_post_meta( $post_id, 'recreate_diensten', true );

		/* If a post class was input, sanitize it and add it to the post class array. */
		if ( !empty( $diensten ) )
			$classes[] = sanitize_html_class( $diensten );
	}

	return $classes;
}