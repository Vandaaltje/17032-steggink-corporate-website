<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#ea5627">
    <title>Steggink | Code Oranje</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">

    <link rel="stylesheet" href="css/single.css">
    <link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="css/animations.css">
    <link rel="stylesheet" href="css/animate.css">

    <!-- owl.carousel -->
    <link rel="stylesheet" href="owlcarousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="owlcarousel/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="owlcarousel/assets/owl.theme.green.min.css">

    <!-- Video.js css -->
    <link href="http://vjs.zencdn.net/5.19.0/video-js.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>

    <script src="owlcarousel/owl.carousel.min.js"></script>
    <script src="owlcarousel/owl.autoplay.js"></script>
</head>
<body class="window-open">

<div id="loading-container">
    <div class="animation">
        <div class="logo">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 94.05 97.2"><title>beeldmerk</title>
                <g id="beeldmerk" data-name="beeldmerk">
                    <g id="layer" data-name="layer">
                        <path d="M85.31,48.6A28.36,28.36,0,0,0,94,28.09C94,12.61,81.83,0,66.85,0A26.66,26.66,0,0,0,47,9,26.7,26.7,0,0,0,27.2,0C12.22,0,0,12.58,0,28.06A28.39,28.39,0,0,0,8.71,48.55,28.34,28.34,0,0,0,0,69.06C0,84.53,12.19,97.12,27.17,97.12A26.69,26.69,0,0,0,47,88.19a26.68,26.68,0,0,0,19.89,9c15,0,27.17-12.59,27.17-28.06A28.36,28.36,0,0,0,85.31,48.6ZM66.85,15a12.91,12.91,0,0,1,12.69,13.1,12.9,12.9,0,0,1-12.11,13l-.54,0-10.6,0h-2l.06-13.07c0-.45,0-.89-.06-1.33A12.81,12.81,0,0,1,66.85,15ZM14.51,28.06A12.92,12.92,0,0,1,27.2,15,12.81,12.81,0,0,1,39.75,26.72c0,.45-.06.9-.06,1.37l0,13L27.17,41c-.3,0-.6,0-.91,0A12.91,12.91,0,0,1,14.51,28.06Zm12.66,54.1a13.09,13.09,0,0,1-.92-26.1c.32,0,.63,0,1,0l12.57,0-.06,13.08c0,.3,0,.6,0,.91A12.83,12.83,0,0,1,27.17,82.16Zm39.71.09A12.84,12.84,0,0,1,54.29,70.08c0-.34.05-.67.05-1l0-13h2l10.56,0,.58,0a13.09,13.09,0,0,1-.55,26.14Z"/>
                    </g>
                </g>
            </svg>
        </div>
        <div class="line"></div>
        <div class="largeLine"></div>
    </div>
    <div class="panel left"></div>
    <div class="panel right"></div>
</div>

<div class="main-container">
    <div class="partnerBadge">
        Code Oranje is onderdeel van <a href="http://www.stegginkinfra.nl/"><img src="images/steggink_infra_logo.png" alt=""></a>
    </div>
    <div class="sidepanel">
        <div class="offerte">
            <i class="fa fa-times"></i>
            <div class="content">
                <div role="form" class="wpcf7" id="wpcf7-f75-o1" lang="nl-NL" dir="ltr">
                    <div class="screen-reader-response"></div>
                    <form action="/wordpress/#wpcf7-f75-o1" method="post" class="wpcf7-form" enctype="multipart/form-data" novalidate="novalidate">
                        <div style="display: none;">
                            <input type="hidden" name="_wpcf7" value="75">
                            <input type="hidden" name="_wpcf7_version" value="4.8.1">
                            <input type="hidden" name="_wpcf7_locale" value="nl_NL">
                            <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f75-o1">
                            <input type="hidden" name="_wpcf7_container_post" value="0">
                            <input type="hidden" name="_wpcf7_nonce" value="26add75486">
                        </div>
                        <h3>Vraag een offerte aan</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusantium cumque, debitis dolores doloribus eveniet id libero minima mollitia nobis non quaerat quibusdam quo saepe similique ullam velit voluptas voluptatum.</p>
                        <p><label> Je naam (verplicht)<br>
                                <span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></span> </label></p>
                        <p><label> Je e-mail (verplicht)<br>
                                <span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false"></span> </label></p>
                        <p><label> Onderwerp<br>
                                <span class="wpcf7-form-control-wrap your-subject"><input type="text" name="your-subject" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false"></span> </label></p>
                        <p><label> Je bericht<br>
                                <span class="wpcf7-form-control-wrap your-message"><textarea name="your-message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false"></textarea></span> </label></p>
                        <p><label> Uiterste offerte datum<br>
                                <span class="wpcf7-form-control-wrap date-653"><input type="date" name="date-653" value="" class="wpcf7-form-control wpcf7-date wpcf7-validates-as-required wpcf7-validates-as-date" aria-required="true" aria-invalid="false"></span> </label></p>
                        <p><label> Bestand(en) uploaden<br>
                                <span class="wpcf7-form-control-wrap file-579"><input type="file" name="file-579" size="40" class="wpcf7-form-control wpcf7-file" aria-invalid="false"></span> </label></p>
                        <p><input type="submit" value="Verzenden" class="wpcf7-form-control wpcf7-submit"><span class="ajax-loader"></span></p>
                        <div class="wpcf7-response-output wpcf7-display-none"></div></form></div>            </div>
        </div>
        <div class="contact active">
            <i class="fa fa-times"></i>
            <div class="content">
                <div id="contactForm">
                    <div role="form" class="wpcf7" id="wpcf7-f7-o2" lang="nl-NL" dir="ltr">
                        <div class="screen-reader-response"></div>
                        <form action="/wordpress/#wpcf7-f7-o2" method="post" class="wpcf7-form" novalidate="novalidate">
                            <div style="display: none;">
                                <input type="hidden" name="_wpcf7" value="7">
                                <input type="hidden" name="_wpcf7_version" value="4.8.1">
                                <input type="hidden" name="_wpcf7_locale" value="nl_NL">
                                <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f7-o2">
                                <input type="hidden" name="_wpcf7_container_post" value="0">
                                <input type="hidden" name="_wpcf7_nonce" value="fa9de86acd">
                            </div>
                            <h2>Neem contact met ons op!</h2>
                            <p><span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Naam"></span><span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Email"></span><span class="wpcf7-form-control-wrap your-subject"><input type="text" name="your-subject" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="Onderwerp"></span><span class="wpcf7-form-control-wrap your-message"><textarea name="your-message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false" placeholder="Uw bericht"></textarea></span><input type="submit" value="Verstuur bericht" class="wpcf7-form-control wpcf7-submit"><span class="ajax-loader"></span></p>
                            <div class="wpcf7-response-output wpcf7-display-none"></div></form></div>                    <a id="mapsBtn" class="btn" href="https://www.google.com/maps?q=52.3203578,6.505219600000032">Bekijk op de kaart</a>
                </div>
            </div>
        </div>
        <div class="projects">
            <i class="fa fa-times"></i>
            <div class="content">
                <h2>Projecten</h2>
                <div class="project">
                    <a id="1234" href="" class="projectImage" style="background-image: url('http://codeoranje.recreate.nl/wordpress/wp-content/uploads/2017/06/Laag-18.png')"></a>
                    <a id="2134" href=""><h3>Bas O.H.</h3></a>
                    <p>Nulla quis lorem ut libero malesuada feugiat Vivamus suscipit tortor eget felis porttitor volutpat Lorem ipsum dolor sit amet, consectetur adipiscing elit Nulla porttitor</p>                        <a class="read-more btn" id="72" href="">Verder lezen</a>
                </div>
                <div class="project">
                    <a id="1235" href="" class="projectImage" style="background-image: url('http://codeoranje.recreate.nl/wordpress/wp-content/uploads/2017/06/Laag-17.png')"></a>
                    <a id="1235" href=""><h3>Bake</h3></a>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit Consectetur quia repudiandae sint velit voluptatem Ad blanditiis dicta neque veniam voluptas Alias aliquid ducimus error</p>                        <a class="read-more btn" id="29" href="">Verder lezen</a>
                </div>
                <div class="project">
                    <a id="1236" href="" class="projectImage" style="background-image: url('http://codeoranje.recreate.nl/wordpress/wp-content/uploads/2017/06/Laag-16.png')"></a>
                    <a id="1236" href=""><h3>MCO AH Triolet</h3></a>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit Consectetur quia repudiandae sint velit voluptatem Ad blanditiis dicta neque veniam voluptas Alias aliquid ducimus error</p>                        <a class="read-more btn" id="18" href="">Verder lezen</a>
                </div>
            </div>
        </div>
    </div>
    <div class="detailed-project">
        <i class="fa fa-angle-right"></i>
        <div class="inner">
            <div class="spinner">
                <div class="double-bounce1"></div>
                <div class="double-bounce2"></div>
            </div>
            <h2></h2>
            <div class="date"></div>
            <img class="projectImage" src="" alt="">
            <div class="content"></div>
        </div>
    </div>
    <div class="panelshade"></div>

    <header class="active">
        <div class="logo">
            <a href="<?php //echo get_bloginfo('url'); ?>"><svg class="toLandingpage" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 271.61 41.67">
                    <defs>
                        <style>.cls-1, .cls-2 {
                                fill: #fff;
                            }

                            .cls-2 {
                                fill-rule: evenodd;
                            }</style>
                    </defs>
                    <title>code_oranje_logo</title>
                    <g id="Laag_2" data-name="Laag 2">
                        <g id="Laag_1-2" data-name="Laag 1">
                            <path class="cls-1" d="M260.62,20.84a7.31,7.31,0,0,0,2.25-5.29,7.13,7.13,0,0,0-7-7.23,6.87,6.87,0,0,0-5.11,2.3,6.88,6.88,0,0,0-5.12-2.31,7.13,7.13,0,0,0-7,7.23,7.32,7.32,0,0,0,2.24,5.28,7.31,7.31,0,0,0-2.25,5.29,7.13,7.13,0,0,0,7,7.23,6.88,6.88,0,0,0,5.11-2.3,6.88,6.88,0,0,0,5.13,2.32,7.13,7.13,0,0,0,7-7.23A7.31,7.31,0,0,0,260.62,20.84Zm-4.76-8.67a3.37,3.37,0,0,1,.15,6.74h-3.38l0-3.37c0-.12,0-.23,0-.34A3.3,3.3,0,0,1,255.86,12.17Zm-13.49,3.37a3.26,3.26,0,0,1,6.51-.34c0,.12,0,.23,0,.35V18.9H245.4A3.33,3.33,0,0,1,242.37,15.54Zm3.26,13.95a3.37,3.37,0,0,1-.24-6.73h3.49l0,3.37c0,.08,0,.15,0,.23A3.31,3.31,0,0,1,245.63,29.49Zm10.24,0a3.31,3.31,0,0,1-3.25-3.14c0-.09,0-.17,0-.26V22.77H256a3.37,3.37,0,0,1-.14,6.74Z"/>
                            <path class="cls-1" d="M252.64,37.76v3.91a20.92,20.92,0,0,0,19-19h-3.91A17,17,0,0,1,252.64,37.76Z"/>
                            <path class="cls-1" d="M267.7,18.93h3.91A20.92,20.92,0,0,0,252.64,0V3.91A17,17,0,0,1,267.7,18.93Z"/>
                            <path class="cls-1" d="M248.88,3.91V0a20.92,20.92,0,0,0-18.93,18.93h3.91A17,17,0,0,1,248.88,3.91Z"/>
                            <path class="cls-1" d="M233.85,22.69h-3.91a20.92,20.92,0,0,0,18.93,19V37.76A17,17,0,0,1,233.85,22.69Z"/>
                            <path class="cls-1" d="M43.4,21.69c0,7.16-5.08,10.44-10.32,10.44C27.36,32.12,23,28.37,23,22S27.12,11.69,33.4,11.69C39.4,11.69,43.4,15.81,43.4,21.69Zm-14.16.2c0,3.36,1.4,5.88,4,5.88,2.36,0,3.88-2.36,3.88-5.88C37.12,19,36,16,33.24,16,30.32,16,29.24,19,29.24,21.89Z"/>
                            <path class="cls-1" d="M66.23,3.29V25.85c0,2.2.08,4.52.16,5.84H61l-.28-2.88h-.08a6.83,6.83,0,0,1-6.12,3.32c-4.64,0-8.36-4-8.36-10,0-6.6,4.08-10.4,8.76-10.4,2.4,0,4.28.84,5.16,2.2h.08V3.29ZM60.16,20.41a10.06,10.06,0,0,0-.08-1.08,3.65,3.65,0,0,0-3.56-3c-2.76,0-4.2,2.48-4.2,5.56,0,3.32,1.64,5.4,4.16,5.4A3.56,3.56,0,0,0,60,24.37a5.21,5.21,0,0,0,.16-1.4Z"/>
                            <path class="cls-2" d="M88.6,20.2c-.62-5.43-3.83-8.48-8.93-8.48-6.12,0-10.33,5-9.58,11.59.61,5.36,4.47,8.73,10,8.73a13.93,13.93,0,0,0,7.93-2.54l0-4.09a12.71,12.71,0,0,1-6.58,2.28,5.17,5.17,0,0,1-5-2.93h0a4.7,4.7,0,0,1-.28-.74v0a10,10,0,0,1,.18-5.32l0-.06.06-.19h0A3.67,3.67,0,0,1,80,15.78a3.58,3.58,0,0,1,3.5,3.37l0,.29H77.89a8.18,8.18,0,0,0-.1,3.44h10.9a18.67,18.67,0,0,0-.09-2.68Z"/>
                            <path class="cls-1" d="M6.4,18.27c0-6,3.76-8.92,8.6-8.92a13.17,13.17,0,0,1,5.1,1V5a18.9,18.9,0,0,0-5.38-.71C6.56,4.31,0,9.43,0,18.63,0,26.3,4.8,32.1,14.12,32.1a20.37,20.37,0,0,0,6-.79V26.19a15.25,15.25,0,0,1-5.06.87C9.6,27.06,6.4,23.67,6.4,18.27Z"/>
                            <path class="cls-1" d="M125.19,17.93c0,8.84-5.36,14.2-13.24,14.2s-12.68-6-12.68-13.72c0-8.08,5.16-14.12,13.12-14.12C120.68,4.29,125.19,10.49,125.19,17.93Zm-19.48.36c0,5.28,2.48,9,6.56,9s6.48-3.92,6.48-9.16c0-4.84-2.32-9-6.52-9S105.72,13,105.72,18.29Z"/>
                            <path class="cls-1" d="M165.11,12.27H171l0,2.68c1.88-2.14,3.69-3.08,6-3.08a6.16,6.16,0,0,1,6.42,5.54,6.17,6.17,0,0,1,.06,1.16l-.07,13.11h-5.9l.06-11.38c0-.36,0-.76-.06-1.09a2.63,2.63,0,0,0-2.69-2.46A4.93,4.93,0,0,0,171,19.08l-.09,12.61H165l.08-19.42Z"/>
                            <path class="cls-1" d="M184.44,35.28A5.39,5.39,0,0,0,188,34c.88-.92,1.2-2.48,1.2-6.36V12.13h6.12v17c0,4.28-1,6.88-2.76,8.56S188,40,185,40Z"/>
                            <path class="cls-2" d="M160.24,13l-.21-.11a24.26,24.26,0,0,0-6.55-.95c-9.5,0-12.54,7-11.31,13.5.76,4,3.64,6.73,7.12,6.73a5.68,5.68,0,0,0,5-2.72V31.7h6V13Zm-5.93,8.77c-.19,4.11-1.88,5.81-3.47,5.81s-2.58-1.31-2.94-3.21c-.68-3.6,1.17-8.12,4.85-8.12a8.6,8.6,0,0,1,1.53.16l0,3.52Z"/>
                            <path class="cls-2" d="M217.6,20.24c-.62-5.43-3.83-8.48-8.93-8.48-6.12,0-10.33,5-9.58,11.59.61,5.36,4.47,8.73,10,8.73a13.93,13.93,0,0,0,7.93-2.54l0-4.09a12.71,12.71,0,0,1-6.58,2.28,5.17,5.17,0,0,1-5-2.93h0a4.7,4.7,0,0,1-.28-.74v0a10,10,0,0,1,.18-5.32l0-.06.06-.19h0A3.67,3.67,0,0,1,209,15.82a3.58,3.58,0,0,1,3.5,3.37l0,.29h-5.66a8.18,8.18,0,0,0-.1,3.44h10.9a18.67,18.67,0,0,0-.09-2.68Z"/>
                            <polygon class="cls-2" points="189.32 4.79 195.36 4.79 195.28 9.46 189.24 9.46 189.32 4.79 189.32 4.79"/>
                            <path class="cls-2" d="M140.85,11.78a7.32,7.32,0,0,0-5.56,2.78l0-2.39h-5.87l-.08,19.42h5.87L135.27,19a4.93,4.93,0,0,1,3.85-2.32,2.65,2.65,0,0,1,1.72.6V11.78Z"/>
                        </g>
                    </g>
                </svg></a>
        </div>
        <ul id="menu">
            <li><a data-href="diensten">Diensten</a></li>
            <li><a data-href="projecten">Projecten</a></li>
            <li><a data-href="over-ons">Over ons</a></li>
            <li><a data-href="contact">Contact</a></li>
        </ul>
        <i class="fa fa-bars hamburger"></i>
    </header>
    <div class="windows">
        <div class="window active parallax" data-order="1">
            <div class="landing parallax__layer parallax__layer--base">
                <div class="summary">
                    <h2 class="title">Code Oranje</h2>
                        <div id="quoteSlider" class="owl-carousel">
                                <div>Lorem ipsum</div>
                                <div>Dolo sit amet</div>
                                <div>straten straten en straten</div>
                        </div>
                    <a data-href="diensten" class="btn">Bekijk onze diensten</a>
                    <div id="watchLandingVideo" class="btn mobile-only">Bekijk video</div>
                </div>
                <div class="scrollerMouse">
                    <img src="images/mouse.png" alt="">
                    <span>Scroll naar beneden</span>
                </div>
            </div>
            <section id="diensten" class="diensten no-padding parallax__layer parallax__layer--base">
                <div class="dienstenMenu">
                    <div class="inner">
                        <div class="subject" data-subject-id="1">
                            <img src="images/beeldmerk_round_white.svg" alt="">
                            <span class="subjectTitle">Advies</span>
                        </div>
                        <div class="subject" data-subject-id="2">
                            <img src="images/beeldmerk_round_white.svg" alt="">
                            <span class="subjectTitle">Alpa</span>
                        </div>
                        <div class="subject" data-subject-id="3">
                            <img src="images/beeldmerk_round_white.svg" alt="">
                            <span class="subjectTitle">Aardevol</span>
                        </div>
                        <div class="subject" data-subject-id="4">
                            <img src="images/beeldmerk_round_white.svg" alt="">
                            <span class="subjectTitle">Advies</span>
                        </div>
                    </div>
                </div>
                <div class="dienstenContentContainer">
                    <div class="indicator"></div>
                    <div class="dienstenContent active" data-subject-id="1">
                        <div class="inner small">
                            <h2>Het beste advies</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur lacinia tristique
                                lacus, in rhoncus purus porta a. Donec a turpis pretium, faucibus velit ac, mollis
                                lorem. Cras id ex viverra, malesuada ex vitae, malesuada ligula.</p>

                            <p>Proin blandit diam eu nunc egestas, a congue ante malesuada. Morbi eget ullamcorper est,
                                laoreet mollis ligula.</p>
                        </div>
                    </div>
                    <div class="dienstenContent" data-subject-id="2">
                        <div class="inner small">
                            <h2>Alpa</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur lacinia tristique
                                lacus, in rhoncus purus porta a. Donec a turpis pretium, faucibus velit ac, mollis
                                lorem. Cras id ex viverra, malesuada ex vitae, malesuada ligula.</p>

                            <p>Proin blandit diam eu nunc egestas, a congue ante malesuada. Morbi eget ullamcorper est,
                                laoreet mollis ligula.</p>
                        </div>
                    </div>
                    <div class="dienstenContent" data-subject-id="3">
                        <div class="inner small">
                            <h2>Aardevol</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur lacinia tristique
                                lacus, in rhoncus purus porta a. Donec a turpis pretium, faucibus velit ac, mollis
                                lorem. Cras id ex viverra, malesuada ex vitae, malesuada ligula.</p>

                            <p>Proin blandit diam eu nunc egestas, a congue ante malesuada. Morbi eget ullamcorper est,
                                laoreet mollis ligula.</p>
                        </div>
                    </div>
                    <div class="dienstenContent" data-subject-id="4">
                        <div class="inner small">
                            <h2>Het beste advies</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur lacinia tristique
                                lacus, in rhoncus purus porta a. Donec a turpis pretium, faucibus velit ac, mollis
                                lorem. Cras id ex viverra, malesuada ex vitae, malesuada ligula.</p>

                            <p>Proin blandit diam eu nunc egestas, a congue ante malesuada. Morbi eget ullamcorper est,
                                laoreet mollis ligula.</p>
                        </div>
                    </div>
                </div>
            </section>
            <section class="fullWidthImage">
                <div class="parallax__layer parallax__layer--back" style="background-image: url('images/Laag 7.png')">
            </section>
            <section id="projecten" class="projects parallax__layer parallax__layer--base">
                <div class="inner">
                    <h2>Projecten</h2>
                    <div class="projectsContainer">
                        <div class="project">
                            <a id="1234" href="" class="projectImage" style="background-image: url('images/Laag 16.png')"></a>
                            <a id="1234" href=""><h3>Lorem ipsum</h3></a>
		                    <?php echo "<p>" . substr("Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum ea eligendi fugiat laboriosam nostrum obcaecati odio sequi velit! Corporis earum eligendi fugiat illo inventore numquam, qui quidem sunt tenetur velit!", 0, 300) . "</p>"; ?>
                            <a class="read-more" id="1234" href="">Verder lezen</a>
                        </div>
                        <div class="project">
                            <a id="1235" href="" class="projectImage" style="background-image: url('images/Laag 17.png')"></a>
                            <a id="1235" href=""><h3>Lorem ipsum</h3></a>
		                    <?php echo "<p>" . substr("Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum ea eligendi fugiat laboriosam nostrum obcaecati odio sequi velit! Corporis earum eligendi fugiat illo inventore numquam, qui quidem sunt tenetur velit!", 0, 300) . "</p>"; ?>
                            <a class="read-more" id="1235" href="">Verder lezen</a>
                        </div>
                        <div class="project">
                            <a id="1236" href="" class="projectImage" style="background-image: url('images/Laag 16.png')"></a>
                            <a id="1236" href=""><h3>Lorem ipsum</h3></a>
		                    <?php echo "<p>" . substr("Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum ea eligendi fugiat laboriosam nostrum obcaecati odio sequi velit! Corporis earum eligendi fugiat illo inventore numquam, qui quidem sunt tenetur velit!", 0, 300) . "</p>"; ?>
                            <a class="read-more" id="1236" href="">Verder lezen</a>
                        </div>
                    </div>
                    <div class="btn" id="allProjects">Bekijk alle projecten</div>
                </div>
            </section>
            <section class="teamPhotoSlider">
                <h2 class="projects parallax__layer parallax__layer--base">Team</h2>
                <div class="parallax__layer parallax__layer--back">
                    <div id="teamPhotoSlider" class="owl-carousel">
                        <img src="images/Laag 24.png"/>
                        <img src="images/Laag 7.png"/>
                    </div>
                </div>
            </section>
            <section id="over-ons" class="about parallax__layer parallax__layer--base">
                <div class="inner">
                    <h2>Even voorstellen</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur atque autem commodi, cum
                        doloribus enim illum numquam odio officia recusandae repudiandae soluta, temporibus totam vitae
                        voluptates. Amet aspernatur eum quos!</p>
                    <div class="team">
                        <div class="member">
                            <div class="picture" style="background-image: url('images/code-oranje/Chiel Waaijer.png');"></div>
                            <div class="info">
                                <div class="name">Chiel Waaijer</div>
                                <div class="function">Functie</div>
                            </div>
                        </div>
                        <div class="member">
                            <div class="picture" style="background-image: url('images/code-oranje/Dirk Deterink.png');"></div>
                            <div class="info">
                                <div class="name">Dirk Deterink</div>
                                <div class="function">Functie</div>
                            </div>
                        </div>
                        <div class="member">
                            <div class="picture" style="background-image: url('images/code-oranje/Florienk Deterink.png');"></div>
                            <div class="info">
                                <div class="name">Florienk Deterink</div>
                                <div class="function">Functie</div>
                            </div>
                        </div>
                        <div class="member">
                            <div class="picture" style="background-image: url('images/code-oranje/Jan Steggink.png');"></div>
                            <div class="info">
                                <div class="name">Jan Steggink</div>
                                <div class="function">Functie</div>
                            </div>
                        </div>
                        <div class="member">
                            <div class="picture" style="background-image: url('images/code-oranje/Jeroen Kuipers.png');"></div>
                            <div class="info">
                                <div class="name">Jeroen Kuipers</div>
                                <div class="function">Functie</div>
                            </div>
                        </div>
                        <div class="member">
                            <div class="picture" style="background-image: url('images/code-oranje/Laurens Harmelink.png');"></div>
                            <div class="info">
                                <div class="name">Laurens Harmelink</div>
                                <div class="function">Functie</div>
                            </div>
                        </div>
                        <div class="member">
                            <div class="picture" style="background-image: url('images/code-oranje/Peter Kamp.png');"></div>
                            <div class="info">
                                <div class="name">Peter Kamp</div>
                                <div class="function">Functie</div>
                            </div>
                        </div>
                        <div class="member">
                            <div class="picture" style="background-image: url('images/code-oranje/Peter Meijer.png');"></div>
                            <div class="info">
                                <div class="name">Peter Meijer</div>
                                <div class="function">Functie</div>
                            </div>
                        </div>
                        <div class="member">
                            <div class="picture" style="background-image: url('images/code-oranje/Roy de Jonge.png');"></div>
                            <div class="info">
                                <div class="name">Roy de Jonge</div>
                                <div class="function">Functie</div>
                            </div>
                        </div>
                        <div class="member">
                            <div class="picture" style="background-image: url('images/code-oranje/Roy Nieuwe Weme.png');"></div>
                            <div class="info">
                                <div class="name">Roy Nieuwe Weme</div>
                                <div class="function">Functie</div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <footer id="contact" class="parallax__layer parallax__layer--base">
                <div class="inner cols">
                    <div class="col">
                        <h3>Contactgegevens</h3>
                        <p>Ootmarsumseweg 320<br/>
                            7667 PC Reutum</p>
                        <a href="tel:0541670720" class="btn">0541670720</a>
                        <a href="mailto:info@stegginkinfra.nl" class="btn">info@stegginkinfra.nl</a>
                        <div class="btn" id="requestContact">Neem contact op</div>
                        <div class="social-media">
                            <a href="http://facebook.com/">
                                <div class="facebook"></div>
                            </a>
                            <a href="http://twitter.com/">
                                <div class="twitter"></div>
                            </a>
                            <a href="http://linkedin.com/">
                                <div class="linkedin"></div>
                            </a>
                        </div>
                    </div>
                    <div id="map1" class="googleMap" data-lat="52.3203578" data-lng="6.505219600000032" data-title="Code Oranje"></div>
                </div>
            </footer>
            <div class="shade"></div>
            <div class="video">
                <video id="video1" class="video-js" preload="auto" width="100%" loop="true" muted="true" data-setup="">
                    <source src="video/CodeOranje_Final.mp4"/>
                    <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web
                        browser that<a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5
                            video</a></p>
                </video>
            </div>
        </div>
    </div>
</div>

<!-- Google Maps API -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDy73PHHhM5POMmBakQzEefFvCpaX63lo4&callback=initMap" async defer></script>

<script>
    var panorama;

    function initMap() {
        if ($('.googleMap').length > 0) {
            $map = $('.googleMap');
            panorama = new google.maps.StreetViewPanorama(
                document.getElementById($map.attr('id')),
                {
                    position: {lat: $map.data('lat'), lng: $map.data('lng')},
                    pov: {heading: 165, pitch: 0},
                    zoom: 1
                });
        }
    }
</script>

<!-- Video.js -->
<script src="http://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>
<script src="http://vjs.zencdn.net/5.19.1/video.js"></script>

<!-- Put in Wordpress functions.php file -->
<script src="js/script.min.js"></script>

</body>
</html>