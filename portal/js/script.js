var players = [], player = null, currentPage, currentSubject, width;

// set mobile class

if($(window).width() < 1024) $('body').addClass('mobile');
width = $(window).width();

$(window).resize(function () {
    if($(window).width() < 1024 && !$('body').is('.mobile')) {
        $('body').addClass('mobile');
    } else if($(window).width() > 1024 && $('body').is('.mobile')) {
        $('body').removeClass('mobile');
    }
    width = $(window).width();
});

// Start loading animation
$('#loading-container .animation .line').animate({width: '400px'}, 3000);
$('#loading-container .animation .largeLine').stop().animate({width: '400px'}, 4000);

$(document).ready(function () {

    // $('*').on('click', function () {
    //     console.log($(this));
    // });

    // Loading animation
    $('#loading-container .animation .largeLine').stop().delay(300).animate({width: '400px'}, 50);
    $('#loading-container .animation .line').stop().animate({width: '400px'}, 50);

    $('#loading-container').delay(1650).queue(function (next) {
        $(this).addClass('play-animation');
        document.getElementsByClassName('line')[0].style.removeProperty('width');
        document.getElementsByClassName('largeLine')[0].style.removeProperty('width');
        next();
    });

    // Hide the #loading-container
    $('#loading-container').delay(3000).queue(function (next) {
        $('.main-container').addClass('loaded');
        $('header').addClass('active');
        $(this).css('display', 'none');
        $('.windows .window').css('z-index', 10);
        next();
    });

    // Init videos
    /*$('.windows .window div.video .video-js').each(function() {
     var id = $(this).attr('id');
     console.log(id);
     var player = videojs(id);
     players.push(player);
     });*/

    bindHover();

    // Open panel click
    $('.windows .window').on('click', function () {
        if(!$(this).is('.active')) {
            $('.main-container').removeClass('loaded');
            $('.title', this).css('opacity', 0);

            $('.summary', this).css('opacity', 0);
            $('.scrollMouse', this).css('opacity', 0);
            $('.windows .window:not(.active)').css('z-index', 10).stop();

            $(this).css('z-index', 100).addClass('active');
            if(!$('body').is('.window-open')) $('body').addClass('window-open');
            $('.window.active .vjs-poster').fadeOut();
            currentPage = $('.windows .window ').data('order');

            $(this).delay(1500).queue(function (next) {
                window.location.href = $(this).data('url');
            });

            /*$('.title', this).delay(300).animate({'opacity': 1}, 300);
            $('.summary', this).delay(300).animate({'opacity': 1}, 300);
            $('.scrollMouse', this).delay(300).animate({'opacity': 1}, 300);
            if(width >= 1024) $('header ul#menu').css('display', 'block').animate({'opacity':1}, 1000);

            $(this).unbind('mouseenter');
            $(this).unbind('mouseleave');

            $(this).delay(1500).queue(function (next) {
                $(this).css('overflow-y', 'scroll');
                $('.window.active .landing').css({'width': '100%', 'position': 'relative'});
                $('.window.active').scrollTop(0);

                // Menu color-scheme change
                $('.window.active').scroll( function () {
                    var scrollTop = $(this).scrollTop();
                    if(scrollTop >= ($(window).height() - 80)) {
                        if(!$('header').is('.small')) {
                            $('header').addClass('small');
                        }
                    } else {
                        if($('header').is('.small')) {
                            $('header').removeClass('small');
                        }
                    }
                });

                $('*[data-href]').on('click', function (e) {
                    e.preventDefault();
                    if($('.window.active').scrollTop() != ($('.window.active section#'+$(this).data('href')).position().top-80) + $('.window.active').scrollTop()) {
                        $('.window.active').stop().animate({
                            scrollTop: ($('.window.active section#' + $(this).data('href')).position().top - 80) + $('.window.active').scrollTop()
                        }, 750, 'swing');
                    }
                });

                if($('.window.active .googleMap').length > 0) {
                    // Load Google maps
                    $map = $('.window.active .googleMap');

                    var gmap = new GMaps({
                        div: "#"+$map.attr('id'),
                        lat: $map.data('lat'),
                        lng: $map.data('lng'),
                        zoom: 15
                    });

                    gmap.setCenter($map.data('lat'), $map.data('lng'))

                    gmap.addMarker({
                        lat: $map.data('lat'),
                        lng: $map.data('lng'),
                        title: $map.data('title'),
                        infoWindow: {
                            content: '<b>' + $map.data('title') + '</b>'
                        }
                    });
                    next();
                }

                currentSubject = 1;
                $('.dienstenMenu .subject').removeClass('active');
                $('.dienstenMenu .subject[data-subject-id='+currentSubject+']').addClass('active');
                currentSubject = $('.windows .window.active .dienstenContentContainer .dienstenContent').data('subject-id');
                moveIndicator(1);
            });*/
        }
    });

    $('.toLandingpage').on('click', function () {

        var id = "video"+currentPage;
        console.log(id);
        player = videojs(id);
        console.log(player);
        player.currentTime(0);
        player.pause();
        // player.posterImage.show();

        currentSubject = 1;
        $('.windows .window.active').addClass('closing');
        if($('body').is('.window-open')) $('body').removeClass('window-open');
        $('header').removeClass('small');
        $('header ul#menu').animate({'opacity':0}, 200, function() { $(this).css('display', 'none'); });
        $('.windows .window').css('overflow-y','unset');
        $('.windows .window.active .landing').css({'width':'auto', 'position':'unset'});
        // $('.windows .window.active .shade').stop().animate({'opacity':0.4});
        $('.scrollMouse', this).css('opacity',0);

        $('.windows .window.active').stop().delay(1000).removeClass('active').stop().delay(1500).queue(function (next) {
            $(this).css('z-index', 10);
            bindHover();
            $('.main-container').addClass('loaded');
            $('.windows .window.closing').removeClass('closing');
            next();
        });
    });


    // TODO: repsonsive animation maken
    // Hover animation diensten
    $('.dienstenMenu .subject').mouseenter(function () {
        var id = $(this).data('subject-id');
        var offset = $(this).offset();
        offset = offset.left + (($(this).width() / 2) - 30);
        $('.window.active .dienstenContentContainer .indicator').stop().animate({'left':offset+"px"});

        $(this).on('click', function () {
            currentSubject = id;
            $('.dienstenMenu .subject').removeClass('active');
            $(this).addClass('active');
            $('.window.active .dienstenContentContainer .dienstenContent.active').removeClass("active");
            $('.window.active .dienstenContentContainer .dienstenContent[data-subject-id="'+id+'"]').addClass("active");
        });
    }).mouseleave(function () {
        moveIndicator(currentSubject);
    });

});

function moveIndicator(position) {
    var offset = $('.window.active .subject[data-subject-id="'+position+'"]').offset();
    offset = offset.left + (($('.window.active .subject[data-subject-id="'+position+'"]').width() / 2) - 30);
    $('.window.active .dienstenContentContainer .indicator').stop().animate({'left':offset+"px"});
}

function bindHover() {
    console.log('hover');
    // Reset inline styles
    // var $shades = document.getElementsByClassName('shade')
    // for (var s = 0; s < $shades.length; s++ ) { $shades[s].style.removeProperty('opacity'); }
    //
    // var $titles = document.querySelectorAll('.window h2.title');
    // for (var t = 0; t < $titles.length; t++) {
    //     console.log("Remove " + t + " title opactiy");
    //     $titles[t].style.removeProperty('opacity');
    // }

    var $summaries = document.querySelectorAll('.window .summary');
    for (var su = 0; su < $summaries.length; su++) {
        console.log("Remove " + su + " summary opactiy");
        $summaries[su].style.removeProperty('opacity');
    }

    // Video Panel hover
    $('.windows .window').mouseenter(function () {
        var id = $('div.video .video-js', this).attr('id');
        console.log("enter " + id);
        player = videojs(id);
        // player.posterImage.hide();
        player.play();

    }).mouseleave(function () {
        var id = $('div.video .video-js', this).attr('id');
        console.log("leave " + id);
        player = videojs(id);
        player.currentTime(0);
        player.pause();
        // player.posterImage.show();
        // $('.vjs-poster').fadeIn();
    });
}

(function ($) {
    $.fn.removeStyle = function (style) {
        var search = new RegExp(style + '[^;]+;?', 'g');

        return this.each(function () {
            $(this).attr('style', function (i, style) {
                return style.replace(search, '');
            });
        });
    };
}(jQuery));