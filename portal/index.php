<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#ea5627">
    <title>Steggink | Code Oranje</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">

    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="css/animations.css">

    <!-- Video.js css -->
    <link href="http://vjs.zencdn.net/5.19.0/video-js.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body class="window-open">

<div id="loading-container">
    <div class="animation">
        <div class="logo">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 94.05 97.2"><title>beeldmerk</title>
                <g id="beeldmerk" data-name="beeldmerk">
                    <g id="layer" data-name="layer">
                        <path d="M85.31,48.6A28.36,28.36,0,0,0,94,28.09C94,12.61,81.83,0,66.85,0A26.66,26.66,0,0,0,47,9,26.7,26.7,0,0,0,27.2,0C12.22,0,0,12.58,0,28.06A28.39,28.39,0,0,0,8.71,48.55,28.34,28.34,0,0,0,0,69.06C0,84.53,12.19,97.12,27.17,97.12A26.69,26.69,0,0,0,47,88.19a26.68,26.68,0,0,0,19.89,9c15,0,27.17-12.59,27.17-28.06A28.36,28.36,0,0,0,85.31,48.6ZM66.85,15a12.91,12.91,0,0,1,12.69,13.1,12.9,12.9,0,0,1-12.11,13l-.54,0-10.6,0h-2l.06-13.07c0-.45,0-.89-.06-1.33A12.81,12.81,0,0,1,66.85,15ZM14.51,28.06A12.92,12.92,0,0,1,27.2,15,12.81,12.81,0,0,1,39.75,26.72c0,.45-.06.9-.06,1.37l0,13L27.17,41c-.3,0-.6,0-.91,0A12.91,12.91,0,0,1,14.51,28.06Zm12.66,54.1a13.09,13.09,0,0,1-.92-26.1c.32,0,.63,0,1,0l12.57,0-.06,13.08c0,.3,0,.6,0,.91A12.83,12.83,0,0,1,27.17,82.16Zm39.71.09A12.84,12.84,0,0,1,54.29,70.08c0-.34.05-.67.05-1l0-13h2l10.56,0,.58,0a13.09,13.09,0,0,1-.55,26.14Z"/>
                    </g>
                </g>
            </svg>
        </div>
        <div class="line"></div>
        <div class="largeLine"></div>
    </div>
    <div class="panel left"></div>
    <div class="panel right"></div>
</div>

<div class="main-container">
    <header>
        <div class="logo">
            <svg class="toLandingpage" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 283.38 41.67">
                <defs>
                    <style>.cls-1, .cls-2 {
                            fill: #fff;
                        }

                        .cls-1 {
                            fill-rule: evenodd;
                        }</style>
                </defs>
                <title>logo_white</title>
                <g id="Laag_2" data-name="Laag 2">
                    <g id="Laag_1-2" data-name="Laag 1">
                        <path class="cls-1" d="M219.45,27.55c-1.58,0-2.58-1.31-2.94-3.21-.68-3.6,1.17-8.12,4.85-8.12a8.6,8.6,0,0,1,1.53.16l0,3.52v1.84c-.19,4.11-1.88,5.81-3.47,5.81Zm9.19-14.69a24.26,24.26,0,0,0-6.55-.95c-9.5,0-12.54,7-11.31,13.5.76,4,3.64,6.73,7.12,6.73a5.68,5.68,0,0,0,5-2.72v2.29h6V13l-.21-.11Zm-19.1-.95A7.32,7.32,0,0,0,204,14.69l0-2.39h-5.87L198,31.72h5.87L204,19.11a4.93,4.93,0,0,1,3.85-2.32,2.65,2.65,0,0,1,1.72.6V11.91Zm-24.25.43,0-1c0-2.34.45-4.75,2.09-6.37a7.39,7.39,0,0,1,5.2-1.82,8.62,8.62,0,0,1,3.1.48l-.14,4.51a5.48,5.48,0,0,0-1.88-.32c-1.86,0-2.7,1.62-2.69,3.56l0,1h3.8V16.7H191l0,15h-5.7l0-15V12.35Zm-22.41,0h5.87l0,2.68c1.88-2.14,3.69-3.08,6-3.08a6.16,6.16,0,0,1,6.42,5.54,6.17,6.17,0,0,1,.06,1.16l-.07,13.11h-5.9l.06-11.38c0-.36,0-.76-.06-1.09a2.63,2.63,0,0,0-2.69-2.46,4.93,4.93,0,0,0-3.85,2.32l-.09,12.61H162.8l.08-19.42ZM153,4.93h5.87l-.08,4.68h-5.87L153,4.93Zm0,7.37h5.87l-.08,19.42h-5.87L153,12.3Z"/>
                        <path class="cls-1" d="M68.49,12.23a31.74,31.74,0,0,0-5-.47c-7.32,0-11.47,4.93-10.57,12.5.55,4.64,3.91,7.86,8.25,7.86a7.26,7.26,0,0,0,3.47-.8,24.87,24.87,0,0,0,.16-4,4.33,4.33,0,0,1-1.39.22c-2.43,0-4.4-2-4.74-4.82-.43-3.62,1.8-6.48,5-6.48a7.68,7.68,0,0,1,2.85.54l0,9.56h0c0,3.62,0,3.8,0,3.8l-.06,1a5.22,5.22,0,0,1-.2,1.41,7.88,7.88,0,0,1-.64,1.59c-.77,1.41-2.9,2.39-5,2.39a14,14,0,0,1-5-1.05,15.32,15.32,0,0,1-1.57-.72l0,4.67a16.72,16.72,0,0,0,3.65,1.16,15.61,15.61,0,0,0,3.67.4c4.31,0,7.53-1.56,9.38-4.56C71.9,34.54,72.3,32.66,72.3,29l.06-15.9c-1.75-.43-2.51-.65-3.87-.87Zm22.47,0a31.74,31.74,0,0,0-5-.47c-7.32,0-11.47,4.93-10.57,12.5.55,4.64,3.91,7.86,8.25,7.86a7.27,7.27,0,0,0,3.47-.8,25,25,0,0,0,.16-4,4.34,4.34,0,0,1-1.39.22c-2.43,0-4.4-2-4.74-4.82-.43-3.62,1.8-6.48,5-6.48a7.69,7.69,0,0,1,2.85.54l0,9.56h0c0,3.62,0,3.8,0,3.8l-.06,1a5.24,5.24,0,0,1-.2,1.41,7.91,7.91,0,0,1-.64,1.59c-.77,1.41-2.9,2.39-5,2.39a14,14,0,0,1-5-1.05,15.46,15.46,0,0,1-1.57-.72l0,4.67a16.71,16.71,0,0,0,3.65,1.16,15.62,15.62,0,0,0,3.67.4c4.31,0,7.53-1.56,9.38-4.56,1.15-1.88,1.54-3.77,1.54-7.43l.06-15.9c-1.75-.43-2.51-.65-3.87-.87ZM30.23,27.31a9.53,9.53,0,0,1-2.24.47h0a2.55,2.55,0,0,1-2.66-2.47c0-.29-.08-.65-.08-1l0-8h4.24l0-4.06h-4.2l0-4.6H19.4l0,4.6v4l0,9.53a6.55,6.55,0,0,0,0,1.41,5.43,5.43,0,0,0,4.37,4.6,7.83,7.83,0,0,0,2.25.3h.24a13.09,13.09,0,0,0,6-1.54,11.23,11.23,0,0,1-2-3.29Zm19.82-7c-.62-5.43-3.83-8.48-8.93-8.48-6.12,0-10.33,5-9.58,11.59.61,5.36,4.47,8.73,10,8.73a13.93,13.93,0,0,0,7.93-2.54l0-4.09A12.71,12.71,0,0,1,43,27.81a5.17,5.17,0,0,1-5-2.93h0a4.7,4.7,0,0,1-.28-.74v0a10,10,0,0,1,.18-5.32l0-.06.06-.19h0a3.67,3.67,0,0,1,3.52-2.63A3.58,3.58,0,0,1,45,19.26l0,.29H39.34a8.18,8.18,0,0,0-.1,3.44h10.9A18.67,18.67,0,0,0,50,20.31ZM137.31,22l-.08,9.71h-5.87l.11-26.12h5.9l-.06,15V22Zm1.86-.75,3.75-9h6.24l-3.9,8.84,4.34,10.58h-6.42l-4-10.46Zm-30.31-9h5.87l0,2.68c1.88-2.14,3.69-3.08,6-3.08a6.16,6.16,0,0,1,6.42,5.54,6.13,6.13,0,0,1,.07,1.16l-.07,13.11h-5.9l.06-11.38c0-.36,0-.76-.06-1.09a2.63,2.63,0,0,0-2.68-2.46,4.93,4.93,0,0,0-3.85,2.32l-.09,12.61h-5.87l.08-19.42ZM98.93,4.93h5.87l-.08,4.68H98.85l.08-4.68Zm0,7.37h5.87l-.08,19.42H98.85l.08-19.42ZM0,25.09a12.6,12.6,0,0,0,2.88,1.7,11,11,0,0,0,3.53.72c2.32,0,3.92-1.45,3.69-3.37-.15-1.23-.94-2.1-2.72-3L4.92,19.8C2.06,18.28.41,16.29.13,13.89-.46,9,3.36,5.16,9,5.16a13,13,0,0,1,4.06.69,17.51,17.51,0,0,1,2.8,1.27l0,5.43c-.56-.47-.93-.8-1.24-1a8.62,8.62,0,0,0-4.73-1.7,3.17,3.17,0,0,0-3.42,3.51c.14,1.2,1,2.21,3.07,3.26l2.12,1.09c3.24,1.67,4.63,3.3,4.91,5.65.61,5.11-3.33,8.8-9.35,8.8A18.32,18.32,0,0,1,3,31.61a17.74,17.74,0,0,1-3-1.16V25.09Z"/>
                        <path class="cls-2" d="M272.39,20.84a7.31,7.31,0,0,0,2.25-5.29,7.13,7.13,0,0,0-7-7.23,6.87,6.87,0,0,0-5.11,2.3,6.88,6.88,0,0,0-5.12-2.31,7.13,7.13,0,0,0-7,7.23,7.32,7.32,0,0,0,2.24,5.28,7.31,7.31,0,0,0-2.25,5.29,7.13,7.13,0,0,0,7,7.23,6.88,6.88,0,0,0,5.11-2.3,6.88,6.88,0,0,0,5.13,2.32,7.13,7.13,0,0,0,7-7.23A7.31,7.31,0,0,0,272.39,20.84Zm-4.76-8.67a3.37,3.37,0,0,1,.15,6.74H264.4l0-3.37c0-.12,0-.23,0-.34A3.3,3.3,0,0,1,267.63,12.17Zm-13.49,3.37a3.26,3.26,0,0,1,6.51-.34c0,.12,0,.23,0,.35V18.9h-3.47A3.33,3.33,0,0,1,254.14,15.54Zm3.26,13.95a3.37,3.37,0,0,1-.24-6.73h3.49l0,3.37c0,.08,0,.15,0,.23A3.31,3.31,0,0,1,257.4,29.49Zm10.24,0a3.31,3.31,0,0,1-3.25-3.14c0-.09,0-.17,0-.26V22.77h3.38a3.37,3.37,0,0,1-.14,6.74Z"/>
                        <path class="cls-2" d="M264.41,37.76v3.91a20.92,20.92,0,0,0,19-19h-3.91A17,17,0,0,1,264.41,37.76Z"/>
                        <path class="cls-2" d="M279.47,18.93h3.91A20.92,20.92,0,0,0,264.41,0V3.91A17,17,0,0,1,279.47,18.93Z"/>
                        <path class="cls-2" d="M260.64,3.91V0a20.92,20.92,0,0,0-18.93,18.93h3.91A17,17,0,0,1,260.64,3.91Z"/>
                        <path class="cls-2" d="M245.62,22.69h-3.91a20.92,20.92,0,0,0,18.93,19V37.76A17,17,0,0,1,245.62,22.69Z"/>
                    </g>
                </g>
            </svg>
        </div>
        <ul id="menu">
            <li><a data-href="diensten">Diensten</a></li>
            <li><a data-href="projecten">Projecten</a></li>
            <li><a data-href="over-ons">Over ons</a></li>
            <li><a data-href="contact">Contact</a></li>
        </ul>
    </header>
    <div class="windows">
        <div class="window" data-order="1" data-url="http://codeoranje.recreate.nl/wordpress/">
            <div class="landing">
                <div class="summary">
                    <h2 class="title">Code oranje</h2>
                    Excellentie in co-creatie.
                </div>
                <div class="scrollerMouse">
                    <img src="images/mouse.png" alt="">
                    <span>Scroll naar beneden</span>
                </div>
            </div>
            <div class="shade"></div>
            <div class="video">
                <video id="video1" class="video-js" preload="auto" width="100%" loop="true" muted="true" data-setup="">
                    <source src="video/CodeOranje_Final.mp4"/>
                    <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web
                        browser that<a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5
                            video</a></p>
                </video>
            </div>
        </div>
        <div class="window" data-order="2">
            <div class="landing">
                <div class="summary">
                    <h2 class="title">Aardevol</h2>
                    <span>Circulaire infra? Mens, Aarde, Geld.</span>
                    <a data-href="diensten" class="btn">Bekijk onze diensten</a>
                </div>
                <div class="scrollerMouse">
                    <img src="images/mouse.png" alt="">
                    <span>Scroll naar beneden</span>
                </div>
            </div>
            <div class="shade"></div>
            <div class="video">
                <video id="video2" class="video-js" preload="auto" width="100%" loop="true" muted="true" data-setup="">
                    <source src="video/Aardevol opening.mp4"/>
                    <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web
                        browser that<a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5
                            video</a></p>
                </video>
            </div>
        </div>
        <div class="window" data-order="3">
            <div class="landing">
                <div class="summary">
                    <h2 class="title">Sinvol</h2>
                    What you see is what you get.
                </div>
                <div class="scrollerMouse">
                    <img src="images/mouse.png" alt="">
                    <span>Scroll naar beneden</span>
                </div>
            </div>
            <div class="shade"></div>
            <div class="video">
                <video id="video3" class="video-js" preload="auto" width="100%" loop="true" muted="true" data-setup="">
                    <source src="video/Sinvol Opening v2.mp4"/>
                    <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web
                        browser that<a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5
                            video</a></p>
                </video>
            </div>
        </div>
    </div>
</div>

<!-- jQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>

<!-- Gmaps.js -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDy73PHHhM5POMmBakQzEefFvCpaX63lo4" async defer></script>
<script src="js/gmaps.js"></script>

<!-- Video.js -->
<script src="http://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>
<script src="http://vjs.zencdn.net/5.19.1/video.js"></script>

<script src="js/script.js"></script>

</body>
</html>